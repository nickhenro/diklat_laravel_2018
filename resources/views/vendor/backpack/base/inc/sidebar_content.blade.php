<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
<li><a href="{{ backpack_url('elfinder') }}"><i class="fa fa-files-o"></i> <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>
<li><a href='{{ backpack_url('tag') }}'><i class='fa fa-tag'></i> <span>Tags</span></a></li>
<li><a href='{{ backpack_url('peminjam') }}'><i class='fa fa-peminjam'></i> <span>Peminjam</span></a></li>
<li><a href='{{ backpack_url('buku_genre') }}'><i class='fa fa-buku_genre'></i> <span>Buku Genre</span></a></li>
<li><a href='{{ backpack_url('buku') }}'><i class='fa fa-buku'></i> <span>Buku</span></a></li>
<li><a href='{{ backpack_url('device_elektronik_type') }}'><i class='fa fa-tag'></i> <span>Device Elektronik Type</span></a></li>
<li><a href='{{ backpack_url('device_elektroniks') }}'><i class='fa fa-tag'></i> <span>Devce Elektronik</span></a></li>