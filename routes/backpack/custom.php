<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    CRUD::resource('buku', 'TagCrudController');
    CRUD::resource('tag', 'TagCrudController');
    CRUD::resource('tag', 'TagCrudController');
    CRUD::resource('buku', 'BukuCrudController');
    CRUD::resource('buku_genre', 'Buku_genreCrudController');
    CRUD::resource('peminjam', 'PeminjamCrudController');
    CRUD::resource('device_elektronik', 'Device_elektronikCrudController');
    CRUD::resource('device_elektronik_type', 'Device_elektronik_typeCrudController');
    CRUD::resource('device_elektroniks', 'Device_elektroniksCrudController');
    CRUD::resource('device_elektronik_types', 'Device_elektronik_typesCrudController');
}); // this should be the absolute last line of this file