<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\Device_elektroniksRequest as StoreRequest;
use App\Http\Requests\Device_elektroniksRequest as UpdateRequest;

/**
 * Class Device_elektroniksCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class Device_elektroniksCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Device_elektroniks');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/device_elektroniks');
        $this->crud->setEntityNameStrings('device_elektroniks', 'device_elektroniks');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->setFromDb();

        $this->crud->addField([
            'label' => 'Pilih Type', // judul inputan
            'type' => 'select',  // type fieldnya
            'name' => 'type_id',  // id form yg tampil
            'entity' =>'Device_elektronik_types', //method yg ada dlm model
            'attribute' => 'type',  //data dari foreign yg mau ditampilkan
            'model' => 'app\Models\Device_elektronik_types' //model yg dipake
        ]);
        
        $this->crud->addColumn([
            'label' => 'Type', 
            'type' => 'select', 
            'name' => 'type_id',
            'entity' =>'Device_elektronik_types',
            'attribute' => 'type',
            'model' => 'app\Models\Device_elektronik_types'
        ]);

        // add asterisk for fields that are required in Device_elektroniksRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
