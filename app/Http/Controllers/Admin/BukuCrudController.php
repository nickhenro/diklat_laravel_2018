<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\BukuRequest as StoreRequest;
use App\Http\Requests\BukuRequest as UpdateRequest;

/**
 * Class BukuCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class BukuCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Buku');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/buku');
        $this->crud->setEntityNameStrings('buku', 'bukus');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->setFromDb();

        //menambah relasi antar table
        $this->crud->addField([
            'label' => 'Pilih Genre', // Label for HTML form field
            'type' => 'select',  // HTML element which displaying transactions
            'name' => 'genre',
            'entity' =>'Buku_genre',
            'attribute' => 'genre',
            'model' => 'app\Models\Buku_genre',
        ],
        'update/create/both'
        );
        
        $this->crud->addColumn([
            'label' => 'genre', // Label for HTML form field
            'type' => 'select',  // HTML element which displaying transactions
            'name' => 'genre',
            'entity' =>'Buku_genre',
            'attribute' => 'genre',
            'model' => 'app\Models\Buku_genre',
        ]);

        // add asterisk for fields that are required in BukuRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
