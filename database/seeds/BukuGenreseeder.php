<?php

use Illuminate\Database\Seeder;

class BukuGenreseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('buku_genres')->insert([
        [    
            'genre'=>'Fiction',
        ],
        [
            'genre'=>'Tutorial',
        ],
        [
            'genre'=>'Novel',
        ],
        [
            'genre'=>'Pelajaran',
        ],
        ]);
    }
}
