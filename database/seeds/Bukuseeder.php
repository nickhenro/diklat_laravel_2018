<?php

use Illuminate\Database\Seeder;

class Bukuseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('bukus')->insert([
            'author'=>'Taufik',
            'title'=>'Belajar PHP',
            'year'=>'2018',
            'genre_id'=>'Tutorial',
        ]);        
    }
}
